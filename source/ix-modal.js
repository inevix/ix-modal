const ixModal = () => {
    const modal = document.querySelector('#ix-modal');

    if (modal) {
        const defaultOptions = {
            width: '320px',
            x: 'left',
            y: 'bottom',
            timeout: 10000
        };
        const dataOptions = {
            width: modal.getAttribute('data-width'),
            x: modal.getAttribute('data-position-x'),
            y: modal.getAttribute('data-position-y'),
            timeout: modal.getAttribute('data-timeout')
        };
        const options = {
            width: dataOptions.width ? `${dataOptions.width}px` : defaultOptions.width,
            x: dataOptions.x ? dataOptions.x : defaultOptions.x,
            y: dataOptions.y ? dataOptions.y : defaultOptions.y,
            timeout: dataOptions.timeout ? parseInt(dataOptions.timeout) * 1000 : defaultOptions.timeout
        };

        const closeBtn = () => {
            const btn = document.createElement('div');

            modal.appendChild(btn);
            btn.className = 'ix-close';

            btn.addEventListener('click', () => modal.style.display = 'none');
        };

        const addStyles = () => {
            modal.style.maxWidth = options.width;

            if (options.x === 'left') {
                modal.style.left = '0';
            } else if (options.x === 'right') {
                modal.style.right = '0';
            }

            if (options.y === 'top') {
                modal.style.top = '0';
            } else if (options.y === 'bottom') {
                modal.style.bottom = '0';
            }

            setTimeout(() => {
                modal.style.display = 'block'
            }, options.timeout);
        };

        closeBtn();
        addStyles();
    }
};

document.addEventListener('DOMContentLoaded', ixModal);