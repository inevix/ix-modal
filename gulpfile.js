'use strict';

let path = {
        public: 'public/',
        source: {
            js: 'source/ix-modal.js',
            css: 'source/ix-modal.scss'
        },
        watch: {
            js: 'source/**/*.js',
            css: 'source/**/*.scss'
        },
        clean: 'public/'
    },
    gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    del = require('del'),
    babel = require('gulp-babel');

function buildCss() {
    return gulp
        .src(path.source.css)
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(cleanCSS())
        .pipe(gulp.dest(path.public));
}

function buildJs() {
    return gulp
        .src(path.source.js)
        .pipe(plumber())
        .pipe(babel({
            presets: ['@babel/env', 'minify'],
            comments: false
        }))
        .pipe(gulp.dest(path.public));
}

function buildClean() {
    return del([path.clean]);
}

function watchFiles() {
    gulp.watch(path.watch.css, buildCss);
    gulp.watch(path.watch.js, buildJs);
}

exports.build = gulp.series(buildClean, gulp.parallel(buildCss, buildJs));
exports.default = watchFiles;