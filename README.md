# IX Modal

JS plugin<br/>
Version: 1.0

Author: <a href="https://t.me/inevix" target="_blank">inevix</a><br/>
URL: <a href="https://inevix.biz/" target="_blank">inevix.biz</a><br/>
E-mail: <a href="mailto:ineviix@gmail.com" target="_blank">ineviix@gmail.com</a><br/>

## How to use:
1. Add the minified css file `ix-modal.css` from the folder `public` to your project into the tag `<head>...</head>`.
<br/><br/>
Example:<br/>
`<link rel="stylesheet" href="public/ix-modal.css">`

2. Add the minified js file `ix-modal.js` from the folder `public` to your project after the closed tag `</body>`.
<br/><br/>
Example:<br/>
`<script async type="text/javascript" src="public/ix-modal.js"></script>`

3. Add the modal block to the any space into the tag `<body>...</body>`.<br/>
You can check the `index.html` file to get an example.
<br/><br/>
Example:<br/>
`<div id="ix-modal"></div>`


Default options without attributes:<br/>
- X position: `left`
- Y position: `bottom`
- Timeout to show modal block after the loaded page (seconds): `10`


## The attributes options:
1. Width: `data-width="[number]"`<br/>
Default value (px): `320`<br/>
Example: `<div id="ix-modal" data-width="266"></div>`

2. X position: `data-position-x="[left|right]"`<br/>
Default value: `left`<br/>
Example: `<div id="ix-modal" data-position-x="right"></div>`

3. Y position: `data-position-y="[top|bottom]"`<br/>
Default value: `bottom`<br/>
Example: `<div id="ix-modal" data-position-y="top"></div>`

4. Timeout to show: `data-timeout="[number]"`<br/>
Default value (seconds): `10`<br/>
Example: `<div id="ix-modal" data-timeout="30"></div>`

Example with all attributes:<br/>
`<div id="ix-modal" data-width="320" data-position-x="left" data-position-y="bottom" data-timeout="10"></div>`

## How to edit:
Run the command in a terminal: `npm i --save-dev`

Run this command to build files: `gulp build`<br/>
or run this command to start watching files: `gulp`

Edit files: `source/ix-modal.scss` and `source/ix-modal.js`
